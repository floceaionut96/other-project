package Tests;

import PokerApp.CombinationsAndChances.ChanceToWinCalculator;
import PokerApp.CombinationsAndChances.PokerCombinations;
import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;

import java.util.ArrayList;
import java.util.List;

public class tests {

    public static void main(String[] args) {
        PokerCombinations pokerCombinations=new PokerCombinations();
        List<Card> cardsOnTable=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Ace),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Ace)));

        List<Card> cardsInHand=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND,CardNumber.King),
                new Card(CardSuit.CLUB,CardNumber.Three)
        ));
        List<Card> cardsOnTable2=new ArrayList<>(List.of(
                new Card(CardSuit.HEART, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Jack),
                new Card(CardSuit.CLUB, CardNumber.Seven)));

        List<Card> cardsInHand2=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND,CardNumber.Six),
                new Card(CardSuit.CLUB,CardNumber.Nine)
        ));

        List<Card> cardsOnTable3=new ArrayList<>(List.of(
                new Card(CardSuit.HEART, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Jack),
                new Card(CardSuit.CLUB, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Three)));

        List<Card> cardsInHand3=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND,CardNumber.Six),
                new Card(CardSuit.CLUB,CardNumber.Nine)
        ));
        List<Card> cardstoplay=new ArrayList<>();
        cardstoplay.addAll(cardsOnTable);
        cardstoplay.addAll(cardsInHand);
        double d= ChanceToWinCalculator.doChanceToWin(cardsOnTable,cardsInHand,pokerCombinations.doHandScore(cardstoplay));
        System.out.println("Chance to win with Hand: "+cardsInHand+" and Table:"+cardsOnTable+":"+d+"%" );
        double d2=ChanceToWinCalculator.doChanceToWin(cardsOnTable2,cardsInHand2,pokerCombinations.doHandScore(getCardsToPlay(cardsOnTable2,cardsInHand2)));
        System.out.println("Chance to win with Hand: "+cardsInHand2+" and Table:"+cardsOnTable2+":"+d2+"%" );
        double d3=ChanceToWinCalculator.doChanceToWin(cardsOnTable3,cardsInHand3,pokerCombinations.doHandScore(getCardsToPlay(cardsOnTable2,cardsInHand2)));
        System.out.println("Chance to win with Hand: "+cardsInHand3+" and Table:"+cardsOnTable3+":"+d3+"%" );
    }

    public static List<Card> getCardsToPlay(List<Card> cardsOnTable,List<Card> cardsInHand){
        List<Card> cardsToPlay=new ArrayList<>();
        cardsToPlay.addAll(cardsInHand);
        cardsToPlay.addAll(cardsOnTable);
        return cardsToPlay;
    }
}
