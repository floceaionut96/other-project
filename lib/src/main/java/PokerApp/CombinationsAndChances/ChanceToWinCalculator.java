package PokerApp.CombinationsAndChances;

import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;
import PokerApp.Deck.Deck;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChanceToWinCalculator {

    private static PokerCombinations pC=new PokerCombinations();
    private static List<Card> listForScore=new ArrayList();

    public static double doChanceToWin(List<Card> cardsOnTable, List<Card> cardsInHand, double score){

        int n=0;
        int numberOfCombinations=0;
        double chance=0;

        Deck deckOfCards= new Deck();

        deckOfCards.getDeckOfCards().removeAll(cardsOnTable);
        deckOfCards.getDeckOfCards().removeAll(cardsInHand);

        for(int i=0 ; i<deckOfCards.getSize()-1;i++){
            for(int j=i+1; j< deckOfCards.getSize();j++){
                numberOfCombinations++;
                listForScore.addAll(cardsOnTable);
                listForScore.add(deckOfCards.getDeckOfCards().get(i));
                listForScore.add(deckOfCards.getDeckOfCards().get(j));
              //  System.out.println(listForScore+"-> "+pC.doHandScore(listForScore));
                if(pC.doHandScore(listForScore)<score){
                    n++;
                }
                listForScore.clear();
            }
        }

        listForScore.clear();
       //  System.out.println("Combinations that are lower="+n);
       //  System.out.println("noOfComb="+numberOfCombinations);
        chance= (100*(double)n)/(double)numberOfCombinations;
        return chance;
    }
}
