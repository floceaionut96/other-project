package PokerApp.CombinationsAndChances;

import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PokerCombinations {
    private CombinationsOrder cO=new CombinationsOrder();

    public  int doHandScore(List<Card> cards){
        if(cards.size()==5){
            return doScore(cards);
        }
        else if(cards.size()==6){
            int max=0;
            for(int i=0;i<cO.getCombinationsFor6().size();i++){
                int value=doScore(new ArrayList<Card>(List.of(
                        cards.get(cO.getCombinationsFor6().get(i).get(0)),
                        cards.get(cO.getCombinationsFor6().get(i).get(1)),
                        cards.get(cO.getCombinationsFor6().get(i).get(2)),
                        cards.get(cO.getCombinationsFor6().get(i).get(3)),
                        cards.get(cO.getCombinationsFor6().get(i).get(4)))));
                if(value>max){
                    max=value;
                }
            }
            return max;
        }
        else if(cards.size()==7){
            int max=0;
            for(int i=0;i<cO.getCombinationsFor7().size();i++){
                int value=doScore(new ArrayList<Card>(List.of(
                        cards.get(cO.getCombinationsFor7().get(i).get(0)),
                        cards.get(cO.getCombinationsFor7().get(i).get(1)),
                        cards.get(cO.getCombinationsFor7().get(i).get(2)),
                        cards.get(cO.getCombinationsFor7().get(i).get(3)),
                        cards.get(cO.getCombinationsFor7().get(i).get(4)))));
                if(value>max){
                    max=value;
                }
            }
            return max;
        }
        return 0;
    }

    public Map<CardSuit, List<Card>> sortSuits(List<Card> cards){
        return cards.stream().collect(Collectors.groupingBy(c-> c.getCardSuit()));
    }


    public Map<CardNumber, List<Card>> sortDuplicates(List<Card> cards){
        return cards.stream().collect(Collectors.groupingBy(c -> c.getCardNumber()));
    }

    int doScore(List<Card> cards){
        List<Card> cardsList=new ArrayList<>();
        cardsList.addAll(cards);
        if(checkStraightFlush(cardsList).size()==5){
            return getCombinationScore("1001",get5CardsValue(cardsList));
        }
        else if(check4s(cardsList).size()==4){
            return getCombinationScore("1000",get5CardsValue(cardsSorted(cardsList,check4s(cardsList))));
        }
        else if(check23s(cardsList).size()==5){
            return getCombinationScore("0111",get5CardsValue(cardsList));
        }
        else if(checkFlush(cardsList).size()==5){
            return getCombinationScore("0110",get5CardsValue(cardsList));
        }
        else if(checkStraight(cardsList).size()==5){
            return getCombinationScore("0101",get5CardsValue(checkStraight(cardsList)));
        }
        else if(check3s(cardsList).size()==3){
            return getCombinationScore("0100",get5CardsValue(cardsSorted(cardsList,check3s(cardsList))));
        }
        else if(check2s(cardsList).size()==4){
            return getCombinationScore("0011",get5CardsValue(cardsSorted(cardsList,check4s(cardsList))));
        }
        else if(check2s(cardsList).size()==2){
            return getCombinationScore("0010",get5CardsValue(cardsSorted(cardsList,check4s(cardsList))));
        }
        else{
            cardsList.sort(new Comparator<Card>() {
                @Override
                public int compare(Card o1, Card o2) {
                    return ((Integer) o2.getCardNumber().getValue()).compareTo(o1.getCardNumber().getValue()); }});
            return getCombinationScore("0001",get5CardsValue(cardsList));
        }
    }

    public List<Card> cardsSorted(List<Card> cards, List<Card> combination){
        List<Card> restCards=cards;
        restCards.removeAll(combination);
        restCards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return ((Integer) o2.getCardNumber().getValue()).compareTo(o1.getCardNumber().getValue()); }});
        combination.addAll(restCards);
        return combination;

    }
    List<Card> check2s(List<Card> cards) {
        List<Card> list=new ArrayList<>();
        for (Map.Entry<CardNumber, List<Card>> entry : sortDuplicates(cards).entrySet()) {
            if(entry.getValue().size()==2) {
                list.addAll(entry.getValue());
            }
        }
        list.sort(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return ((Integer)o2.getCardNumberValue()).compareTo(o1.getCardNumberValue());
            }
        });
        return list;
    }
    public List<Card> check3s (List<Card> cards){
        List<Card> list=new ArrayList<>();
        for (Map.Entry<CardNumber, List<Card>> entry : sortDuplicates(cards).entrySet()) {
            if(entry.getValue().size()==3) {
                list.addAll(entry.getValue());
            }
        }
        return list;
    }
    public List<Card> check4s (List<Card> cards){
        List<Card> list=new ArrayList<>();
        for (Map.Entry<CardNumber, List<Card>> entry : sortDuplicates(cards).entrySet()) {
            if(entry.getValue().size()==4) {
                list.addAll(entry.getValue());
            }
        }
        return list;
    }
    public List<Card> check23s (List<Card> cards){
        List<Card> list=new ArrayList<>();
        if(check2s(cards).size()==2 && check3s(cards).size()==3){
            list.addAll(check3s(cards));
            list.addAll(check2s(cards));
        }
        return list;
    }
    public List<Card> checkFlush(List<Card> cards){
        List<Card> list=new ArrayList<>();
        for (Map.Entry<CardSuit, List<Card>> entry : sortSuits(cards).entrySet()) {
            if(entry.getValue().size()==5){
                list.addAll(entry.getValue());
            }
        }
        list.sort(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return ((Integer)o2.getCardNumberValue()).compareTo(o1.getCardNumberValue());
            }
        });
        return list;
    }

    public List<Card> checkStraight(List<Card> cards){
        List<Card> list=new ArrayList<>();
        cards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return ((Integer) o2.getCardNumber().getValue()).compareTo(o1.getCardNumber().getValue()); }});
        if(cards.get(0).getCardNumber().getValue()==14 &&
                cards.get(1).getCardNumber().getValue()==5 &&
                cards.get(2).getCardNumber().getValue()==4 &&
                cards.get(3).getCardNumber().getValue()==3 &&
                cards.get(4).getCardNumber().getValue()==2){
            list.add(cards.get(1));
            list.add(cards.get(2));
            list.add(cards.get(3));
            list.add(cards.get(4));
            list.add(cards.get(0));
            return list;
        }
        for(int i =1 ; i<cards.size();i++){
            if(cards.get(i-1).getCardNumber().getValue()-cards.get(i).getCardNumber().getValue()!=1){
                return list;
            }
        }
        return cards;
    }

    public List<Card> checkStraightFlush(List<Card> cards){
        List<Card> list=new ArrayList<>();
        cards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return ((Integer) o2.getCardNumber().getValue()).compareTo(o1.getCardNumber().getValue()); }});
        if(checkStraight(cards).size()==5 && checkFlush(cards).size()==5){
            return cards;
        }
        return list;
    }


    public String get5CardsValue(List<Card> cards){
        String n="";
        for(int i=0;i< cards.size() ;i++){
            n=n+binaryCardValue(cards.get(i).getCardNumberValue());
        }
        return n;
    }

    public int getCombinationScore(String combinationValue , String cardsValue){
        String s=combinationValue+cardsValue;
        return Integer.parseInt(s,2);
    }

    public static String binaryCardValue(int n){
        String s=Integer.toBinaryString(n);
        if(s.length()==1){
            return "000"+s;
        }
        else if(s.length()==2){
            return "00"+s;
        }
        else if(s.length()==3){
            return "0"+s;
        }
        else{
            return s;
        }
    }
    }

