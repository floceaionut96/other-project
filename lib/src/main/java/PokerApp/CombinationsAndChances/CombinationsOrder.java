package PokerApp.CombinationsAndChances;

import java.util.ArrayList;
import java.util.List;

public class CombinationsOrder {

    private List<List<Integer>> combinationsFor6;
    private List<List<Integer>> combinationsFor7;

    public CombinationsOrder() {
        this.combinationsFor6 =new ArrayList<>();
        combinationsFor6.add(new ArrayList<>(List.of(0, 1, 2, 3, 4)));
        combinationsFor6.add(new ArrayList<>(List.of(0, 1, 2, 3, 5)));
        combinationsFor6.add(new ArrayList<>(List.of(0, 1, 2, 4, 5)));
        combinationsFor6.add(new ArrayList<>(List.of(0, 1, 3, 4, 5)));
        combinationsFor6.add(new ArrayList<>(List.of(0, 2, 3, 4, 5)));
        combinationsFor6.add(new ArrayList<>(List.of(1, 2, 3, 4, 5)));
        this.combinationsFor7 =new ArrayList<>();
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 3, 4)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 3, 5)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 3, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 4, 5)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 4, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 2, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 3, 4, 5)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 3, 4, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 3, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 1, 4, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 2, 3, 4, 5)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 2, 3, 4, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 2, 3, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 2, 4, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(0, 3, 4, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(1, 2, 3, 4, 5)));
        combinationsFor7.add(new ArrayList<>(List.of(1, 2, 3, 4, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(1, 2, 3, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(1, 2, 4, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(1, 3, 4, 5, 6)));
        combinationsFor7.add(new ArrayList<>(List.of(2, 3, 4, 5, 6)));
    }

    public List<List<Integer>> getCombinationsFor6() {
        return combinationsFor6;
    }

    public List<List<Integer>> getCombinationsFor7() {
        return combinationsFor7;
    }
}
