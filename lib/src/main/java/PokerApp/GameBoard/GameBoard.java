package PokerApp.GameBoard;

import PokerApp.CombinationsAndChances.PokerCombinations;
import PokerApp.Deck.Card;
import PokerApp.Deck.Deck;
import PokerApp.Player.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GameBoard {
    private List<Player> playersInGame;
    private List<Card> cardsOnTable;
    private int blind=10;
    private static int moneyToCall=10;
    private static int roundsPlayed=0;
    private PokerCombinations pC=new PokerCombinations();

    public GameBoard() {
        playersInGame=new ArrayList<>();
        cardsOnTable=new ArrayList<>();
    }

    public void putCardsOnTable(){
        if(cardsOnTable.size()==0){
            for(int i=0;i<3;i++){
                cardsOnTable.add(Deck.getCardFromTheDeck());
            }
        }
        else{
            cardsOnTable.add(Deck.getCardFromTheDeck());
        }
    }

    public void resetPlayerHands(){
        for(int i=0;i<playersInGame.size();i++){
            playersInGame.get(i).resetHand();
        }
    }
    public void resetGameBoardAndRemovePlayer(){
        cardsOnTable.clear();
        roundsPlayed++;
        moneyToCall=10;
        resetStatusForAllPlayers();
        reorderPlayers();
        resetPlayerHands();
        resetPlayersBets();
        for(int i=0 ; i<playersInGame.size()-1 ; i++){
            if(playersInGame.get(i).getBank()<=10){
                playersInGame.remove(i);
            }
        }
    }

    public void resetPlayersBets(){
        for(int i=0;i<playersInGame.size();i++){
            playersInGame.get(i).resetMoneyPlayedThisRound();
        }
    }

    public Player humanPlayer(){
        for(int i=0 ; i<playersInGame.size(); i++){
            if(playersInGame.get(i) instanceof HumanPlayer){
                return playersInGame.get(i);
            }
        }
        return playersInGame.get(0);
    }

    public int totalMoneyOnTable(){
        int total=0;
        for(int i=0;i<playersInGame.size();i++){
            total=total+playersInGame.get(i).getMoneyPlayedThisRound();
        }
        return total;
    }

    public List<Player> playersWhoNotFolded(){
        List<Player> list=new ArrayList<>();
        list=playersInGame.stream()
                .filter(p-> p.getStatus().equals(Status.FOLDED)==false)
                .collect(Collectors.toList());
        return list;
    }

    public boolean checkIfWinner(){
        if(playersWhoNotFolded().size()==1){
            System.out.println("All players folded.Round is over!"+playersWhoNotFolded().get(0).getName()+" won "+this.totalMoneyOnTable());
            playersWhoNotFolded().get(0).receiveMoney(totalMoneyOnTable());
            return true;
        }
        System.out.println("There are still "+playersWhoNotFolded().size()+" players in the game!");
        return false;

    }

    public void addPlayer(String name){
        Player player=new HumanPlayer(name);
        playersInGame.add(player);
    }

    public void addComputerPlayers(){
        ComputerPlayer Computer1=new ComputerPlayer("Computer1");
        ComputerPlayer Computer2=new ComputerPlayer("Computer2");
        ComputerPlayer Computer3=new ComputerPlayer("Computer3");
        playersInGame.add(Computer1);
        playersInGame.add(Computer2);
        playersInGame.add(Computer3);
    }

    public void reorderPlayers(){
        playersInGame.sort(new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return ((Integer)o1.getOrderNo()).compareTo(o2.getOrderNo());
            }
        });
    }

    public void resetStatusIfPlayerRaise(String playername){
        for(int i=0;i<playersInGame.size();i++){
            if(playersInGame.get(i).getName().equals(playername)==false && playersInGame.get(i).getStatus().equals(Status.FOLDED)==false){
                playersInGame.get(i).setStatus(Status.NONE);
            }
        }
    }

    public void resetStatusForAllPlayers(){
        for(int i=0;i<playersInGame.size();i++){
                playersInGame.get(i).setStatus(Status.NONE);
        }
    }

    public void giveCardsToPlayers(){
        for(int i=0;i<2;i++){
            for(int j=0;j<playersInGame.size();j++){
                playersInGame.get(j).getACard(Deck.getDeckOfCards().get(0));
                Deck.getDeckOfCards().remove(0);
            }
        }
    }

    public boolean allPlayerCalled(){
        for(int i=0;i<playersInGame.size();i++){
            if(playersInGame.get(i).getStatus()==Status.NONE){
                return false;
            }
        }
        return true;
    }

    public void actionPreFlop(){
        int i=3;
        while(allPlayerCalled()==false){
            if(cardsOnTable.size()==0) {
                if (playersInGame.get(i).getStatus().equals(Status.NONE)) {
                    System.out.println(playersInGame.get(i).getName() + " 's move:");
                    System.out.println("His hand is: "+playersInGame.get(i).getPlayerHand().getCardsInHand());
                    System.out.println("Payed this round:" + playersInGame.get(i).getMoneyPlayedThisRound());
                    Moving.makeAMove(this, playersInGame.get(i), pC.doHandScore(playersInGame.get(i).getPlayerHand().getCardsInHand()));
                }
            }
            System.out.println("Players status: "+getPlayersStatus());
            i++;
            if(i==4){
                i=0;
            }
        }
    }

    public void actionPostFlop(){
        int i=1;
        while(allPlayerCalled()==false){

                if (playersInGame.get(i).getStatus().equals(Status.NONE)) {
                    System.out.println(playersInGame.get(i).getName() + " 's move:");
                    System.out.println("Payed this round:" + playersInGame.get(i).getMoneyPlayedThisRound());
                    Moving.makeAMove(this, playersInGame.get(i), pC.doHandScore(playersInGame.get(i).getPlayerHand().getCardsInHand()));
                }

            i++;
            if(i==4){
                i=0;
            }
        }
    }

    public String getPlayersStatus(){
        String s="";
        for(int i=0;i<playersInGame.size();i++){
            s=s+playersInGame.get(i).getName()+"="+playersInGame.get(i).getStatus()+";";
        }
        return  s;
    }

    public static void increaseMoneyToCall(int amount){
        moneyToCall=moneyToCall+amount;
    }

    public void payBlinds(){
        if(playersInGame.size()==2){
            System.out.println(playersInGame.get(0).getName()+" is Dealer and big blind and pay "+10);
            playersInGame.get(0).makeAPayment(10);
            System.out.println(playersInGame.get(1).getName()+" is small blind and pay "+5);
            playersInGame.get(1).makeAPayment(5);
        }
        else {
            System.out.println(playersInGame.get(0).getName() + " is Dealer!");
            System.out.println(playersInGame.get(1).getName() + " is small blind and pay " + 5);
            playersInGame.get(1).makeAPayment(5);
            System.out.println(playersInGame.get(2).getName() + " is big blind and pay " + 10);
            playersInGame.get(2).makeAPayment(10);
        }
    }

    public void setOrder(){
        if(roundsPlayed==0){
            for(int i=0;i<playersInGame.size();i++){
                playersInGame.get(i).setOrderNo(i);
            }
        }
        else{
            for(int i=0;i<playersInGame.size();i++){
                if(playersInGame.get(i).getOrderNo()==playersInGame.size()-1){
                    playersInGame.get(i).setOrderNo(0);
                }
                else{
                    playersInGame.get(i).setOrderNo(playersInGame.get(i).getOrderNo()+1);
                }
            }
        }
    }

    public List<Player> getPlayersInGame() {
        return playersInGame;
    }

    public List<Card> getCardsOnTable() {
        return cardsOnTable;
    }

    public static int getMoneyToCall() {
        return moneyToCall;
    }

    public String getPlayersName(){
        String s="[";
        for(int i=0;i<playersInGame.size();i++){
            s=s+playersInGame.get(i).getName()+",";
        }
        return s+"\b"+"]";
    }


}
