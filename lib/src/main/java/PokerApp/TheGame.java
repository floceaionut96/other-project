package PokerApp;

import PokerApp.CombinationsAndChances.PokerCombinations;
import PokerApp.Deck.Deck;
import PokerApp.GameBoard.GameBoard;

import java.util.Scanner;

public class TheGame {
    //Create Deck
    //Create needed instances
    //Shuffle deck
    //Create player with name
    //Create computer players
    //Set Order
    //Pay blinds
    //Give Cards to players
    //Show player cards
    //Actions preflop
    //Show pot
    //Show flop
    //reset players status
    //actions on preTurn card
    //reset players status
    //put turn
    //actions on preRiver card
    //reset players status
    //put river
    //actions on postrier card
    //check for winner
    //give winner money
    //...
    //increase roundNo +1
    //reset variables
    public static void main(String[] args) {

        //Create Deck
        Deck deck=new Deck();

        //Create needed instances
        GameBoard gameBoard=new GameBoard();
        PokerCombinations pokerCombinations=new PokerCombinations();

        //Shuffle Deck
        deck.ShuffleDeck();
        System.out.println("Deck Shuffled: "+deck.showDeck());

        //Create player with name
        Scanner s=new Scanner(System.in);
        System.out.println("Enter your name: ");
        String name=s.next();
        gameBoard.addPlayer(name);

        //Create computer players
        gameBoard.addComputerPlayers();

        while(gameBoard.getPlayersInGame().size()>1) {
            //Set order
            gameBoard.setOrder();

            //Pay blinds
            gameBoard.payBlinds();

            //Give cards to players
            gameBoard.giveCardsToPlayers();

            //Show player cards
            System.out.println("Your cards: " + gameBoard.humanPlayer().getPlayerHand());

            //Players action PREFLOP
            gameBoard.actionPreFlop();

            //Show pot
            System.out.println("Total money to win now: " + gameBoard.totalMoneyOnTable());


            //Check for winner
            if(gameBoard.checkIfWinner()==true){
                //resets
                Deck.resetDeck();
                deck.ShuffleDeck();
                gameBoard.resetGameBoardAndRemovePlayer();
                continue;
            }

            //Show FLOP CARDS
            gameBoard.putCardsOnTable();
            System.out.println("FLOP IS: " + gameBoard.getCardsOnTable());

            //reset players status
            gameBoard.resetStatusForAllPlayers();

            //actions preTurn
            gameBoard.actionPostFlop();

            //Check for winner
            if(gameBoard.checkIfWinner()==true){
                //resets
                Deck.resetDeck();
                deck.ShuffleDeck();
                gameBoard.resetGameBoardAndRemovePlayer();
                continue;
            }

            //show flop+turn
            gameBoard.putCardsOnTable();
            System.out.println("Cards on table: " + gameBoard.getCardsOnTable());

            //reset players status
            gameBoard.resetStatusForAllPlayers();

            //actions preRiver
            gameBoard.actionPostFlop();

            //Check for winner
            if(gameBoard.checkIfWinner()==true){
                //resets
                Deck.resetDeck();
                deck.ShuffleDeck();
                gameBoard.resetGameBoardAndRemovePlayer();
                continue;
            }

            //show flop+turn+river
            gameBoard.putCardsOnTable();
            System.out.println("Cards on table: " + gameBoard.getCardsOnTable());

            //reset players status
            gameBoard.resetStatusForAllPlayers();

            //Last action
            gameBoard.actionPostFlop();

            //Check for winner
            if(gameBoard.checkIfWinner()==true){
                //resets
                Deck.resetDeck();
                deck.ShuffleDeck();
                gameBoard.resetGameBoardAndRemovePlayer();
            }
        }

    }
}
