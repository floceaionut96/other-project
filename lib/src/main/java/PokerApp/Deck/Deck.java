package PokerApp.Deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

    private static List<Card> DeckOfCards;

    public Deck() {
        DeckOfCards=new ArrayList<>();
        for(CardSuit c : CardSuit.values()){
            for(CardNumber cn : CardNumber.values()){
                DeckOfCards.add(new Card(c,cn));
            }
        }
        removeOtherAces();
    }

    public static void removeOtherAces(){
        for(int i=0;i<DeckOfCards.size();i++){
            if(DeckOfCards.get(i).getCardNumber().getValue()==1){
                DeckOfCards.remove(i);
            }
        }
    }

    public static Card getCardFromTheDeck(){
        Card card=getDeckOfCards().get(0);
        getDeckOfCards().remove(0);
        return card;
    }

    public static void resetDeck(){
        DeckOfCards.clear();
        for(CardSuit c : CardSuit.values()){
            for(CardNumber cn : CardNumber.values()){
                DeckOfCards.add(new Card(c,cn));
            }
        }
        removeOtherAces();
    }



    public static void ShuffleDeck(){
        Collections.shuffle(DeckOfCards);
    }

    public static List<Card> getDeckOfCards() {
        return DeckOfCards;
    }

    public static int getSize(){
        return getDeckOfCards().size();
    }


    public static String showDeck() {
        String s="Deck: ";
        for(Card c:DeckOfCards){
            s=s+c.toString()+" ";
        }
        return s+";";
    }
}
