package PokerApp.Deck;

import java.util.Objects;

public class Card {
    CardNumber cardNumber;
    CardSuit cardSuit;

    public Card(CardSuit cardSuit, CardNumber cardNumber) {
        this.cardNumber = cardNumber;
        this.cardSuit = cardSuit;
    }

    @Override
    public String toString() {
        if(cardNumber.getValue()==11){
            return "{" +
                    "J" +
                    " " + cardSuit+
                    '}';
        }
        else if(cardNumber.getValue()==12){
            return "{" +
                    "Q" +
                    " " + cardSuit+
                    '}';
        }
        else if(cardNumber.getValue()==13){
            return "{" +
                    "K" +
                    " " + cardSuit+
                    '}';
        }
        else if(cardNumber.getValue()==14){
            return "{" +
                    "A" +
                    " " + cardSuit+
                    '}';
        }
        else {
            return "{" +
                    cardNumber.getValue() +
                    " " + cardSuit +
                    '}';
        }
    }

    public CardNumber getCardNumber() {
        return cardNumber;
    }

    public int getCardNumberValue(){
        return this.cardNumber.getValue();
    }

    public CardSuit getCardSuit() {
        return cardSuit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardNumber == card.cardNumber &&
                cardSuit == card.cardSuit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, cardSuit);
    }
}
