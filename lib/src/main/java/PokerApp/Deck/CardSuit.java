package PokerApp.Deck;

public enum CardSuit {
    DIAMOND,
    CLUB,
    HEART,
    SPADE
}
