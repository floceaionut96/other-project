package PokerApp.Player;

import PokerApp.Deck.Card;

public interface Player {

    public String getName();

    public void setName(String name);

    public int getBank();

    public void setBank(int bank);

    public PlayerHand getPlayerHand();

    public void setPlayerHand(PlayerHand playerHand);

    public int getMoneyPlayedThisRound();

    public void setMoneyPlayedThisRound(int moneyPlayedThisRound);

    public Status getStatus();

    public void setStatus(Status status);

    public int getOrderNo();

    public void setOrderNo(int orderNo);

    public void makeAPayment(int amount);

    public void getACard(Card card);

    public void receiveMoney(int amount);

    public void resetHand();

    public void resetMoneyPlayedThisRound();

}
