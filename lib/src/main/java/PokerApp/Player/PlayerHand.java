package PokerApp.Player;

import PokerApp.Deck.Card;

import java.util.ArrayList;
import java.util.List;

public class PlayerHand {
    private List<Card> cardsInHand;

    public PlayerHand() {
        this.cardsInHand =new ArrayList<>();
    }

    public List<Card> getCardsInHand() {
        return cardsInHand;
    }

    public double getHandValue(){
        double score=rankCard(getMaxCardFromHand());
        if(cardsHaveSameValue(getFirstCard(),getSecondCard())){
            if(getFirstCard().getCardNumberValue()==2){
                return 5;
            }
            return rankCard(getFirstCard())*2;
        }
        else{
            if(cardsHaveSameSuit(getFirstCard(),getSecondCard())){
                score=score+2;
            }
            if(getMaxCardFromHand().getCardNumberValue()-getMinCardFromHand().getCardNumberValue()==2
                    && getMaxCardFromHand().getCardNumberValue()<12){
                score=score+1;
            }
            if(getMaxCardFromHand().getCardNumberValue()-getMinCardFromHand().getCardNumberValue()==2){
                score=score-1;
                if(getMaxCardFromHand().getCardNumberValue()<12){
                    score=score+1;
                }
            }
            else if(getMaxCardFromHand().getCardNumberValue()-getMinCardFromHand().getCardNumberValue()==3){
                score=score-2;
            }
            else if(getMaxCardFromHand().getCardNumberValue()-getMinCardFromHand().getCardNumberValue()==4){
                score=score-4;
            }
            else if(getMaxCardFromHand().getCardNumberValue()-getMinCardFromHand().getCardNumberValue()>=5){
                score=score-5;
            }
        }
        return Math.round((double)score);
    }


    public double rankCard(Card card){
        if(card.getCardNumber().getValue()==14){
            return 10;
        }
        else if(card.getCardNumber().getValue()==13){
            return 8;
        }
        else if(card.getCardNumber().getValue()==12){
            return 7;
        }
        else if(card.getCardNumber().getValue()==11){
            return 6;
        }
        else{
            return (double)card.getCardNumberValue()/2;
        }
    }

    public boolean cardsHaveSameValue(Card c1,Card c2){
        if(c1.getCardNumberValue()==c2.getCardNumberValue()){
            return true;
        }
        return false;
    }
    public boolean cardsHaveSameSuit(Card c1,Card c2){
        if(c1.getCardSuit().equals(c2.getCardSuit())){
            return true;
        }
        return false;
    }


    public Card getFirstCard(){
        return this.cardsInHand.get(0);
    }
    public Card getSecondCard(){
        return this.cardsInHand.get(1);
    }


    public Card getMaxCardFromHand(){
        if(getFirstCard().getCardNumberValue()>=getSecondCard().getCardNumberValue()){
            return getFirstCard();
        }
        else{
            return getSecondCard();
        }
    }
    public Card getMinCardFromHand(){
        if(getFirstCard().getCardNumberValue()<=getSecondCard().getCardNumberValue()){
            return getFirstCard();
        }
        else{
            return getSecondCard();
        }
    }


    public void addCardInHand(Card card){
        this.cardsInHand.add(card);
    }
    public void clearHand(){
        this.cardsInHand.clear();
    }

    @Override
    public String toString() {
        String s="[";
        for(Card c:this.cardsInHand){
            s=s+c.toString()+", ";
        }
        return s +"\b"+"\b"+"]";
    }
}
