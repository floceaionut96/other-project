package PokerApp.Player;

import PokerApp.Deck.Card;

public class HumanPlayer implements  Player {
    private String name;
    private int bank=150;
    private PlayerHand playerHand;
    private int moneyPlayedThisRound=0;
    private Status status=Status.NONE;
    private int orderNo=0;

    public HumanPlayer(String name) {
        this.name = name;
        this.playerHand=new PlayerHand();
        this.bank=150;
        this.status=Status.NONE;
    }

    public void getACard(Card card){
        playerHand.addCardInHand(card);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBank() {
        return bank;
    }

    public void setBank(int bank) {
        this.bank = bank;
    }

    public PlayerHand getPlayerHand() {
        return playerHand;
    }

    public void setPlayerHand(PlayerHand playerHand) {
        this.playerHand = playerHand;
    }

    public int getMoneyPlayedThisRound() {
        return moneyPlayedThisRound;
    }

    public void setMoneyPlayedThisRound(int moneyPlayedThisRound) {
        this.moneyPlayedThisRound = moneyPlayedThisRound;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }
    public void makeAPayment(int amount){
        moneyPlayedThisRound=moneyPlayedThisRound+amount;
        bank=bank-amount;
    }

    @Override
    public void resetHand() {
        playerHand.clearHand();
    }

    @Override
    public void receiveMoney(int amount) {
        this.bank=bank+amount;
    }

    @Override
    public void resetMoneyPlayedThisRound() {
        this.moneyPlayedThisRound=0;
    }
}
