package PokerApp.Player;

import PokerApp.CombinationsAndChances.ChanceToWinCalculator;
import PokerApp.GameBoard.GameBoard;

import java.util.Random;
import java.util.Scanner;

public class Moving {
    public static void makeAMove(GameBoard gameBoard, Player player, double score) {
        if (player instanceof ComputerPlayer) {
            Random rnd = new Random();
            int n = rnd.nextInt(6) + 1;
            if (n == 1 || n == 2) {
                doAMovePassive(gameBoard, player, score);
            } else if (n == 3 || n == 4) {
                doAMoveMedium(gameBoard, player, score);
            } else {
                doAMoveAggressive(gameBoard, player, score);
            }
        } else {
            if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                System.out.println("Choose your next move!");
                System.out.println("1.Check  2.Call   3.Raise");
                Scanner scanner = new Scanner(System.in);
                int n = scanner.nextInt();
                if (n == 1) {
                    CHECK(player);
                } else if (n == 2) {
                    CALL(player, gameBoard);
                } else if (n == 3) {
                    RAISE(player, gameBoard);
                }
            } else {
                System.out.println("Choose your next move!");
                System.out.println("1.Fold  2.Call   3.Raise");
                Scanner scanner = new Scanner(System.in);
                int n = scanner.nextInt();
                if (n == 1) {
                    FOLD(player);
                } else if (n == 2) {
                    CALL(player, gameBoard);
                } else if (n == 3) {
                    RAISE(player, gameBoard);
                }
            }

        }
    }

    private static void doAMovePassive(GameBoard gameBoard, Player player, double score) {
        if (gameBoard.getCardsOnTable().size() < 3) {
            if (player.getPlayerHand().getHandValue() < 5) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else {
                    FOLD(player);
                }
            } else if (player.getPlayerHand().getHandValue() >= 5 && player.getPlayerHand().getHandValue() < 10) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                }
                if (player.getMoneyPlayedThisRound() >= player.getBank() / 7) {
                    FOLD(player);
                } else {
                    CALL(player,gameBoard);
                }
            } else if (player.getPlayerHand().getHandValue() >= 10) {
                RAISE(player, gameBoard);
            }
        } else {
            if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 70) {
                RAISE(player, gameBoard);
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 55) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 6) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 40) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 8) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else {
                    FOLD(player);
                }
            }
        }
    }

    private static void doAMoveMedium(GameBoard gameBoard, Player player, double score) {
        if (gameBoard.getCardsOnTable().size() < 3) {
            if (player.getPlayerHand().getHandValue() < 5) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    RAISE(player, gameBoard);
                } else {
                    FOLD(player);
                }
            } else if (player.getPlayerHand().getHandValue() >= 5 && player.getPlayerHand().getHandValue() < 10) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    RAISE(player, gameBoard);
                }
                if (player.getMoneyPlayedThisRound() >= player.getBank() / 5) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (player.getPlayerHand().getHandValue() >= 10) {
                RAISE(player, gameBoard);
            }
        } else {
            if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 70) {
                RAISE(player, gameBoard);
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 55) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 4) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 40) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 6) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else {
                    FOLD(player);
                }
            }
        }
    }

    private static void doAMoveAggressive(GameBoard gameBoard, Player player, double score) {
        if (gameBoard.getCardsOnTable().size() < 3) {
            if (player.getPlayerHand().getHandValue() < 5) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    RAISE(player, gameBoard);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (player.getPlayerHand().getHandValue() >= 5 && player.getPlayerHand().getHandValue() < 10) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                }
                if (player.getMoneyPlayedThisRound() >= player.getBank() / 3) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (player.getPlayerHand().getHandValue() >= 10) {
                RAISE(player, gameBoard);
            }
        } else {
            if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 70) {
                RAISE(player, gameBoard);
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 55) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    RAISE(player, gameBoard);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 4) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else if (ChanceToWinCalculator.doChanceToWin(gameBoard.getCardsOnTable(), player.getPlayerHand().getCardsInHand(), score) >= 40) {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    RAISE(player, gameBoard);
                } else if (player.getMoneyPlayedThisRound() >= player.getBank() / 4) {
                    FOLD(player);
                } else {
                    CALL(player, gameBoard);
                }
            } else {
                if (gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() == 0) {
                    CHECK(player);
                } else {
                    FOLD(player);
                }
            }
        }
    }

    public static void FOLD(Player player) {
        System.out.println("FOLD!");
        player.setStatus(Status.FOLDED);
    }
    public static void CHECK(Player player){
        System.out.println("CHECK!");
        player.setStatus(Status.CALLED);
    }
    public static void CALL(Player player, GameBoard gameBoard){
        System.out.println("CALL "+(gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound())+"!");
        player.makeAPayment(gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound());
        player.setStatus(Status.CALLED);
    }
    public static void RAISE(Player player, GameBoard gameBoard){
        System.out.println("RAISE!");
        player.makeAPayment(gameBoard.getMoneyToCall() - player.getMoneyPlayedThisRound() + 10);
        GameBoard.increaseMoneyToCall(10);
        gameBoard.resetStatusIfPlayerRaise(player.getName());
        player.setStatus(Status.CALLED);
    }
}
