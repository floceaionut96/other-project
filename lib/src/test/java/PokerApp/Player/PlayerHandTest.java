package PokerApp.Player;

import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;
import junit.framework.TestCase;

public class PlayerHandTest extends TestCase {
    PlayerHand playerHand=new PlayerHand();
    Card c1=new Card(CardSuit.DIAMOND, CardNumber.Ace);
    Card c2=new Card(CardSuit.CLUB, CardNumber.Ace);
    Card c3=new Card(CardSuit.DIAMOND,CardNumber.Nine);
    Card c4=new Card(CardSuit.SPADE,CardNumber.Queen);
    Card c5=new Card(CardSuit.SPADE,CardNumber.Two);
    Card c6=new Card(CardSuit.CLUB, CardNumber.Two);
    Card c7=new Card(CardSuit.HEART,CardNumber.Three);
    Card c8=new Card(CardSuit.HEART, CardNumber.Seven);

    public void testGetHandValue1() {
        playerHand.addCardInHand(c1);
        playerHand.addCardInHand(c2);
     //   assertEquals(20, playerHand.getHandValue());
        System.out.println(playerHand.getHandValue()==20.0);
    }

    public void testGetHandValue2() {
        playerHand.clearHand();
        playerHand.addCardInHand(c1);
        playerHand.addCardInHand(c4);
        assertEquals(9.0,playerHand.getHandValue());
    }

    public void testGetHandValue3() {
        playerHand.clearHand();
        playerHand.addCardInHand(c2);
        playerHand.addCardInHand(c5);
        assertEquals(5.0,playerHand.getHandValue());
    }

    public void testGetHandValue4() {
        playerHand.clearHand();
        playerHand.addCardInHand(c3);
        playerHand.addCardInHand(c8);
        assertEquals(6.0,playerHand.getHandValue());
    }

    public void testGetHandValue5() {
        playerHand.clearHand();
        playerHand.addCardInHand(c7);
        playerHand.addCardInHand(c8);
        assertEquals(2.0, playerHand.getHandValue());
    }

    public void testGetHandValue6() {
        playerHand.clearHand();
        playerHand.addCardInHand(c6);
        playerHand.addCardInHand(c5);
        assertEquals(5.0, playerHand.getHandValue());
    }

    public void testGetHandValue7() {
        playerHand.clearHand();
        playerHand.addCardInHand(c5);
        playerHand.addCardInHand(c8);
        assertEquals(-1.0, playerHand.getHandValue());
    }

    public void testRankCard() {
        System.out.println(playerHand.rankCard(c8));
    }
}