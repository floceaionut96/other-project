package PokerApp.CombinationsAndChances;

import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class PokerCombinationsTest extends TestCase {

    public void testDoScore() {
        PokerCombinations pc=new PokerCombinations();
        List<Card> cards=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Jack),
                new Card(CardSuit.DIAMOND,CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Five)));
        List<Card> cards1=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Ace),
                new Card(CardSuit.DIAMOND,CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Five)));
        List<Card> cards2=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Ten),
                new Card(CardSuit.DIAMOND,CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Five)));
        List<Card> cards3=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Two),
                new Card(CardSuit.CLUB,CardNumber.Two)));
        List<Card> cards4=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Five)));
        List<Card> cards5=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Seven)));
        List<Card> cards6=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Eight),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards7=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Seven),
                new Card(CardSuit.DIAMOND,CardNumber.Six),
                new Card(CardSuit.CLUB,CardNumber.Seven)));
        List<Card> cards8=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards9=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Four),
                new Card(CardSuit.DIAMOND,CardNumber.Three),
                new Card(CardSuit.HEART, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Ace)));
        List<Card> cards10=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Four),
                new Card(CardSuit.DIAMOND,CardNumber.Three),
                new Card(CardSuit.HEART, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Six)));
        List<Card> cards11=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Nine),
                new Card(CardSuit.DIAMOND,CardNumber.Seven),
                new Card(CardSuit.CLUB,CardNumber.Jack)));
        List<Card> cards12=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.DIAMOND, CardNumber.Ace),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.DIAMOND,CardNumber.King)));
        List<Card> cards13=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Jack),
                new Card(CardSuit.DIAMOND, CardNumber.Ace),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.DIAMOND,CardNumber.King)));
        List<Card> cards14=new ArrayList<>(List.of(
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.HEART,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Ace),
                new Card(CardSuit.HEART,CardNumber.Five),
                new Card(CardSuit.HEART,CardNumber.King)));
        List<Card> cards15=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.HEART,CardNumber.Ten),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards16=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Jack),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.HEART,CardNumber.Jack),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards17=new ArrayList<>(List.of(
                new Card(CardSuit.SPADE, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Eight),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards18=new ArrayList<>(List.of(
                new Card(CardSuit.SPADE, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Eight),
                new Card(CardSuit.HEART, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Six),
                new Card(CardSuit.CLUB,CardNumber.Eight)));
        List<Card> cards19=new ArrayList<>(List.of(
                new Card(CardSuit.SPADE, CardNumber.Nine),
                new Card(CardSuit.DIAMOND,CardNumber.Nine),
                new Card(CardSuit.HEART, CardNumber.Nine),
                new Card(CardSuit.DIAMOND,CardNumber.Five),
                new Card(CardSuit.CLUB,CardNumber.Nine)));
        List<Card> cards20=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Eight),
                new Card(CardSuit.DIAMOND,CardNumber.Ten),
                new Card(CardSuit.DIAMOND, CardNumber.Nine),
                new Card(CardSuit.DIAMOND,CardNumber.Jack),
                new Card(CardSuit.DIAMOND,CardNumber.Seven)));
        List<Card> cards21=new ArrayList<>(List.of(
                new Card(CardSuit.DIAMOND, CardNumber.Two),
                new Card(CardSuit.DIAMOND,CardNumber.Four),
                new Card(CardSuit.DIAMOND, CardNumber.Five),
                new Card(CardSuit.DIAMOND,CardNumber.Ace),
                new Card(CardSuit.DIAMOND,CardNumber.Three)));
        List<Card> cards22=new ArrayList<>(List.of(
                new Card(CardSuit.HEART, CardNumber.Queen),
                new Card(CardSuit.HEART,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Jack),
                new Card(CardSuit.HEART,CardNumber.King),
                new Card(CardSuit.HEART,CardNumber.Ace)));
        List<Card> cards23=new ArrayList<>(List.of(
                new Card(CardSuit.HEART, CardNumber.Queen),
                new Card(CardSuit.HEART,CardNumber.Ten),
                new Card(CardSuit.HEART, CardNumber.Jack),
                new Card(CardSuit.HEART,CardNumber.King),
                new Card(CardSuit.CLUB,CardNumber.Ace)));

        System.out.println(cards);
        System.out.println("0:Nothing: "+pc.doHandScore(cards));
        System.out.println();
        System.out.println(cards1);
        System.out.println("1:Nothing "+pc.doHandScore(cards1));
        System.out.println();
        System.out.println(cards2);
        System.out.println("2:Pair 10: "+pc.doHandScore(cards2));
        System.out.println();
        System.out.println( cards3);
        System.out.println("3: 2pairs 7and 2: "+pc.doHandScore(cards3));
        System.out.println();
        System.out.println(cards4);
        System.out.println("4: 2 pairs 7 5: "+pc.doHandScore(cards4));
        System.out.println();
        System.out.println(cards5);
        System.out.println("5: 3x7 : "+pc.doHandScore(cards5));
        System.out.println();
        System.out.println(cards6);
        System.out.println("6: 3x8 "+pc.doHandScore(cards6));
        System.out.println();
        System.out.println(cards7);
        System.out.println("7: 3x7 "+pc.doHandScore(cards7));
        System.out.println();
        System.out.println(cards8);
        System.out.println("8: 3x8 "+pc.doHandScore(cards8));

        System.out.println("//////////////////////////////");

        System.out.println();
        System.out.println(cards9);
        System.out.println("9: str A-5 "+pc.doHandScore(cards9));

        System.out.println();
        System.out.println(cards10);
        System.out.println("10: str 2-6 "+pc.doHandScore(cards10));

        System.out.println();
        System.out.println(cards23);
        System.out.println("23: str 10-A:" +pc.doHandScore(cards23));

        System.out.println();
        System.out.println(cards11);
        System.out.println("11: str 7-J "+pc.doHandScore(cards11));

        System.out.println();
        System.out.println(cards12);
        System.out.println("12: flush "+pc.doHandScore(cards12));

        System.out.println();
        System.out.println(cards13);
        System.out.println("13: flush "+pc.doHandScore(cards13));

        System.out.println();
        System.out.println(cards14);
        System.out.println("14: flush "+pc.doHandScore(cards14));

        System.out.println();
        System.out.println(cards15);
        System.out.println("15: full 8 10 "+pc.doHandScore(cards15));

        System.out.println();
        System.out.println(cards16);
        System.out.println("16: full 8 J "+pc.doHandScore(cards16));

        System.out.println();
        System.out.println(cards17);
        System.out.println("17: qud 8 5 "+pc.doHandScore(cards17));

        System.out.println();
        System.out.println(cards18);
        System.out.println("18: qud 8 6 "+pc.doHandScore(cards18));

        System.out.println();
        System.out.println(cards19);
        System.out.println("19: qud 9 5 "+pc.doHandScore(cards19));

        System.out.println();
        System.out.println(cards20);
        System.out.println("20: RF 7-J "+pc.doHandScore(cards20));

        System.out.println();
        System.out.println(cards21);
        System.out.println("21: RF A-5 "+pc.doHandScore(cards21));

        System.out.println();
        System.out.println(cards22);
        System.out.println("22: RF 10-A "+pc.doHandScore(cards22));
    }

}