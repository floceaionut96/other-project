package PokerApp.CombinationsAndChances;

import PokerApp.Deck.Card;
import PokerApp.Deck.CardNumber;
import PokerApp.Deck.CardSuit;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChanceToWinCalculatorTest extends TestCase {

    PokerCombinations pokerCombinations=new PokerCombinations();
    List<Card> cardsOnTable=new ArrayList<>(List.of(
            new Card(CardSuit.DIAMOND, CardNumber.Ace),
            new Card(CardSuit.DIAMOND,CardNumber.Ten),
            new Card(CardSuit.HEART, CardNumber.Ace)));

    List<Card> cardsInHand=new ArrayList<>(List.of(
            new Card(CardSuit.DIAMOND,CardNumber.King),
            new Card(CardSuit.CLUB,CardNumber.Three)
    ));
    List<Card> cardsOnTable2=new ArrayList<>(List.of(
            new Card(CardSuit.HEART, CardNumber.Two),
            new Card(CardSuit.DIAMOND,CardNumber.Jack),
            new Card(CardSuit.CLUB, CardNumber.Seven)));

    List<Card> cardsInHand2=new ArrayList<>(List.of(
            new Card(CardSuit.DIAMOND,CardNumber.Six),
            new Card(CardSuit.CLUB,CardNumber.Nine)
    ));

    List<Card> cardsOnTable3=new ArrayList<>(List.of(
            new Card(CardSuit.HEART, CardNumber.Two),
            new Card(CardSuit.DIAMOND,CardNumber.Jack),
            new Card(CardSuit.CLUB, CardNumber.Seven),
            new Card(CardSuit.DIAMOND,CardNumber.Three)));

    List<Card> cardsInHand3=new ArrayList<>(List.of(
            new Card(CardSuit.DIAMOND,CardNumber.Six),
            new Card(CardSuit.CLUB,CardNumber.Nine)
    ));
    public List<Card> getCardsToPlay(List<Card> cardsOnTable,List<Card> cardsInHand){
        List<Card> cardsToPlay=new ArrayList<>();
        cardsToPlay.addAll(cardsInHand);
        cardsToPlay.addAll(cardsOnTable);
        return cardsToPlay;
    }
    @Test
    public void testChanceToWin(){
        double d=ChanceToWinCalculator.doChanceToWin(cardsOnTable,cardsInHand,pokerCombinations.doHandScore(getCardsToPlay(cardsOnTable,cardsInHand)));
        System.out.println("Chance to win with Hand:Kd 3c and Table:Ad 10d Ah: "+d+"%" );
        /*double d2=ChanceToWinCalculator.doChanceToWin(cardsOnTable2,cardsInHand2,pokerCombinations.doHandScore(getCardsToPlay(cardsOnTable2,cardsInHand2)));
        System.out.println("Chance to win with Hand:6d 9c and Table:2h Jd 7c: "+d2+"%" );
        double d3=ChanceToWinCalculator.doChanceToWin(cardsOnTable3,cardsInHand3,pokerCombinations.doHandScore(getCardsToPlay(cardsOnTable2,cardsInHand2)));
        System.out.println("Chance to win with Hand:6d 9c and Table:2h Jd 7c: "+d3+"%" );*/
    }

}