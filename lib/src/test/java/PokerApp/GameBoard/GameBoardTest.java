package PokerApp.GameBoard;

import PokerApp.Player.ComputerPlayer;
import PokerApp.Player.HumanPlayer;
import PokerApp.Player.Player;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class GameBoardTest extends TestCase {
    GameBoard gameboard=new GameBoard();

    public void test1(){

        gameboard.addPlayer("Ionut");
        gameboard.addComputerPlayers();
        gameboard.setOrder();
        System.out.println(gameboard.getPlayersName());
        gameboard.getPlayersInGame().get(0).setOrderNo(3);
        gameboard.getPlayersInGame().get(1).setOrderNo(0);
        gameboard.getPlayersInGame().get(2).setOrderNo(1);
        gameboard.getPlayersInGame().get(3).setOrderNo(2);
        gameboard.reorderPlayers();
        for(int i=0;i<gameboard.getPlayersInGame().size();i++){
            System.out.println(gameboard.getPlayersInGame().get(i).getName()+" "+gameboard.getPlayersInGame().get(i).getOrderNo());
        }
        System.out.println(gameboard.getPlayersName());

    }

}